# jyl_ubuntu_install

See 
* https://github.com/MichaelAquilina/ubuntu-ansible
* https://gist.github.com/rbq/886587980894e98b23d0eee2a1d84933



```bash
ansible-playbook setup_jyl.yml -i local.inventory --ask-become-pass
```


To be added manually

For android studio :

```bash
sudo usermod -aG plugdev $LOGNAME
```

For visual studio code

```bash
sudo apt-get install wget gpg
wget -qO- https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > packages.microsoft.gpg
sudo install -D -o root -g root -m 644 packages.microsoft.gpg /etc/apt/keyrings/packages.microsoft.gpg
echo "deb [arch=amd64,arm64,armhf signed-by=/etc/apt/keyrings/packages.microsoft.gpg] https://packages.microsoft.com/repos/code stable main" |sudo tee /etc/apt/sources.list.d/vscode.list > /dev/null
rm -f packages.microsoft.gpg
```

in ~/.bashrc 

alias sail="./vendor/bin/sail"

## For Intellij idea (or other IDEs)

Ref : https://intellij-support.jetbrains.com/hc/en-us/articles/15268113529362-Inotify-Watches-Limit-Linux

```bash
cd /etc/sysctl.d/
sudo vi idea.conf
sudo sysctl -p --system
sysctl fs.inotify.max_user_watches

```

## Tower packages

### Disable trust microphone webcam
ref: https://rietta.com/blog/block-webcam-audio-ubuntu-linux/

 ```bash
 lsusb
 sudo vim /etc/udev/rules.d/90-block-webcam-sound.rules
 sudo udevadm control --reload-rules
 ```

File content : 

SUBSYSTEM=="usb", DRIVER=="snd-usb-audio", ATTRS{idVendor}=="0c45", ATTRS{idProduct}=="6340", ATTR{authorized}="0"


## Gnome extensions

https://github.com/G-dH/advanced-alttab-window-switcher


## Other software 

- https://dbeaver.io/ : sudo snap install dbeaver-ce

## XP-pen

ref : https://www.xp-pen.fr/download-448.html

sudo dpkg -i XPPenLinux4.0.7-250117.deb 

###
Teams



## Elemaent

```bash
sudo apt install -y wget apt-transport-https
sudo wget -O /usr/share/keyrings/element-io-archive-keyring.gpg https://packages.element.io/debian/element-io-archive-keyring.gpg
echo "deb [signed-by=/usr/share/keyrings/element-io-archive-keyring.gpg] https://packages.element.io/debian/ default main" | sudo tee /etc/apt/sources.list.d/element-io.list
sudo apt update
sudo apt install element-desktop
```